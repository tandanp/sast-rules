# yamllint disable
# License: The GitLab Enterprise Edition (EE) license (the “EE License”)
# yamllint enable
---
rules:
- id: "java_deserialization_rule-JacksonUnsafeDeserialization"
  languages:
  - "java"
  severity: "WARNING"
  references:
  - "https://cowtowncoder.medium.com/jackson-2-10-safe-default-typing-2d018f0ce2ba"
  - "https://cowtowncoder.medium.com/on-jackson-cves-dont-panic-here-is-what-you-need-to-know-54cd0d6e8062"
  - "https://www.github.com/mbechler/marshalsec/blob/master/marshalsec.pdf?raw=true"
  - "https://github.com/FasterXML/jackson-databind/issues/2587"
  - "https://cowtowncoder.medium.com/jackson-2-11-features-40cdc1d2bdf3"
  - "https://fasterxml.github.io/jackson-databind/javadoc/2.11/com/fasterxml/jackson/databind/jsontype/impl/LaissezFaireSubTypeValidator.html"
  patterns:
  - pattern-either:
    - patterns:
      - pattern-either:
        - pattern: |
            @JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS,...)
              $TYPE $VAR;
        - pattern: |
            @JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.MINIMAL_CLASS,...)
            $TYPE $VAR;
      - metavariable-regex:
          metavariable: "$TYPE"
          regex: "(Object|Serializable|Comparable|Cloneable|Closeable|AutoCloseable|Handler|Referenceable|DataSource)"
    - pattern: |
        (com.fasterxml.jackson.databind.ObjectMapper $OM).enableDefaultTyping(...);
    - pattern: "(com.fasterxml.jackson.databind.ObjectMapper.DefaultTypeResolverBuilder
        $RB).init(com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CLASS, ...);"
    - pattern: "(com.fasterxml.jackson.databind.ObjectMapper.DefaultTypeResolverBuilder
        $RB).init(com.fasterxml.jackson.annotation.JsonTypeInfo.Id.MINIMAL_CLASS,
        ...);"
    - patterns:
      - pattern: "(com.fasterxml.jackson.databind.ObjectMapper $OM).activateDefaultTyping($B.builder(...).
          ... .allowIfBaseType($TYPE.class). ... );"
      - metavariable-regex:
          metavariable: "$TYPE"
          regex: "(Object|Serializable|Comparable|Cloneable|Closeable|AutoCloseable|Handler|Referenceable|DataSource)"
    - pattern: "(com.fasterxml.jackson.databind.ObjectMapper $OM).activateDefaultTyping((com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator
        $LFSTV), ObjectMapper.DefaultTyping.EVERYTHING);"
  - pattern-not-inside: |
      (com.fasterxml.jackson.databind.ObjectMapper $OM).enable(MapperFeature.BLOCK_UNSAFE_POLYMORPHIC_BASE_TYPES); ...
  - pattern-not-inside: |
      JsonMapper.builder(...).enable(MapperFeature.BLOCK_UNSAFE_POLYMORPHIC_BASE_TYPES).build(); ...
  message: |
    Jackson deserialization vulnerability in Java arises when applications
    using the Jackson library deserialize untrusted JSON data without proper safeguards,
    potentially leading to severe security risks like remote code execution (RCE).
    
    1. Avoid using polymorphic type handling and avoid deserializing user input.
    2. Absolutely avoid using Unsafe Base Types for fields. Types considered unsafe
    base types include:    
      * java.lang.Object    
      * java.io.Closeable    
      * java.io.Serializable
      * java.lang.AutoCloseable    
      * java.lang.Cloneable    
      * java.util.logging.Handler
      * javax.naming.Referenceable    
      * javax.sql.DataSource  
    List of types compiled from a set of all known deserialization "gadgets", types they implement.
    Reference : https://github.com/FasterXML/jackson-databind/issues/2587 
    3. For explicit per-type/per-property polymorphic handling (@JsonTypeInfo), don’t
    use:    
    * @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS) annotation     
    * @JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS) annotation
    Instead use:    
    * @JsonTypeInfo(use = JsonTypeInfo.Id.NAME) annotation where possible.
    4. Use Safe Default Typing feature properly. Don’t use the deprecated ‘enableDefaultTyping()’.    
    * Refer: https://cowtowncoder.medium.com/jackson-2-10-safe-default-typing-2d018f0ce2ba
    * Use Type Validators properly.   
    * Don’t explicitly allow unsafe base types
    5. Avoid using  com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator
    * Does not do any validation, allows all subtypes. Only used for backwards-compatibility
    reasons: users should usually NOT use such a permissive implementation but use
    allow-list/criteria - based implementation.    
    * Refer: https://fasterxml.github.io/jackson-databind/javadoc/2.11/com/fasterxml/jackson/databind/jsontype/impl/LaissezFaireSubTypeValidator.html
    6. Use MapperFeature.BLOCK_UNSAFE_POLYMORPHIC_BASE_TYPES where possible.    
    * Refer: https://cowtowncoder.medium.com/jackson-2-11-features-40cdc1d2bdf3
    7. Regularly update to the latest version of Jackson library.
  metadata:
    shortDescription: "Java Unsafe Jackson Deserialization"
    category: "security"
    cwe: "CWE-502"
    technology:
    - "jackson"
